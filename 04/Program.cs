﻿using Npgsql;

var cs = "Host=127.0.0.1;Username=root;Password=root;Database=root";

using var con = new NpgsqlConnection(cs);
con.Open();

string sql = "SELECT * FROM names";
using var cmd = new NpgsqlCommand(sql, con);

using NpgsqlDataReader rdr = cmd.ExecuteReader();

while (rdr.Read())
{
    Console.WriteLine("{0} {1}", rdr.GetString(0),rdr.GetString(1));
}
