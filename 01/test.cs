﻿using System;

enum TrafficLight
{
	Red,
    Amber,
	Green
}

struct MyName
{
	public string first;
	public string last;
}

public class MyName2
{
	public string first;
	public string last;
	public string total ()
    {
        return first+last;
    }
}


class MyClass
{
    static void Main()
    {   
		string s;
		double a,b;
		s=Console.ReadLine();
		a=double.Parse(s);
		b=a*2.0;
        Console.WriteLine(b);
		TrafficLight t;
		t=TrafficLight.Red;
		Console.WriteLine(t);
		MyName n;
		n.first="Alain";
		n.last="Devos";
		Console.WriteLine(n.first);
		Console.WriteLine(n.last);
		const int SIZE=10;
		MyName[] names=new MyName[SIZE];
		MyName2 n2=new MyName2();
		n2.first="Alain";
		n2.last="De Vos";
		Console.WriteLine(n2.first);
		Console.WriteLine(n2.last);
        Console.WriteLine(n2.total());
    }
}
